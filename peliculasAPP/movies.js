
const APIKey = 'b4dbe2806193890b95c33a2528a1cada';
const url = 'https://api.themoviedb.org/3/';

let SearchComp = {

    template: `
        <div>
        
            <form @submit.prevent="search" class="form-inline md-form form-sm mt-0">

                <div class="input-group md-form form-sm form-2 pl-0 w-100">

                    <div v-show="query" class="input-group-append">

                        <span @click="reiniciaBuscador" class="btn btn-danger"> X </span>

                    </div>
                    <input class="form-control my-0 py-1 green-border" type="text"
                        placeholder="Buscar" aria-label="Buscar" v-model="query">

                    <div class="input-group-append">

                        <button class="btn btn-success">Buscar</button>

                    </div>

                </div>

            </form>

        </div>`,//fin-template

    data(){

        return{

            query: '',
            pagina: 1,

        }//fin-data-return

    },//fin-data

    methods:{

        search(){
            let URL = `${url}search/movie?api_key=${APIKey}&query=${this.query}&page=${this.pagina}`

            fetch(URL)
                .then(res => res.json())
                .then(data => {
                    this.$emit('input', data)
                })
        },//fin-search

        cambioPagina( numPagina ){

            this.pagina = numPagina;
            this.search();

        },//fin-cambioPagina

        reiniciaBuscador() {

            this.query = "";
            this.$emit( 'input', {} );

        }//fin-ReiniciaBuscador

    }//fin-methods

}//FIN-SearchComp


let MovieDetails = {

    name: 'MovieDetails',
    template: `
        <div v-if="Object.keys(movie).length">

            <div class="heroMovie text-white py-5" 
                :style="{
                    'background': 'linear-gradient(rgba(59, 168, 119, 0.45), rgba(59, 168, 119, 1)), url(https://image.tmdb.org/t/p/w1400_and_h450_face'+movie.backdrop_path+')',
                    'background-size': 'cover' }"
            >

                <div class="container">

                    <div class="row align-items-center">

                        <div class="col-12 col-md-4 col-lg-3">

                            <img :src="prefijoImagen + movie.poster_path" class="w-100" />

                        </div>

                        <div class="col-12 col-md-8 col-lg-9">

                            <h2>Detalles: {{ movie.title }}</h2>
                            <p v-text="movie.overview"></p>

                        </div>

                    </div>

                </div>

            </div>

        </div>`,//fin-template
    
    data() {

        return {

            movie: {},
            prefijoImagen: "https://image.tmdb.org/t/p/w185_and_h278_bestv2"

        }//fin-data-return

    },//fin-data

    methods:{

        getMovie() {

            fetch( url + 'movie/' + this.$route.params.id + '?api_key=' + APIKey ).
                then( res => res.json() ).
                then( dataRes => {

                    this.movie = dataRes;

                } );//fin-fetch

        }//fin-getMovie

    },//fin-methods
    
    mounted() {

        this.getMovie();

    }//fin-mounted

}//FIN-MovieDetails



const MovieApp = Vue.component( 'movies-app', {

    template: `
            <div class="container">

                <SearchComp ref="searchComp" v-model="searchMovies"/>

                <div v-show="!Object.keys(searchMovies).length">

                    <div class="row">
                    
                        <div class="col-12 col-md-6 col-lg-4" v-for="( movie, key ) in movies">

                            <div class="card" style="width: 18rem;" :id="movie.id">

                                <img class="card-img-top" :src="prefijoImagen + movie.poster_path">

                                <div class="card-body">

                                    <h3 class="card-title" v-text="movie.title"></h3>
                                    <p class="card-text" v-text="movie.overview.substr( 0, 50 )"></p>
                                    <a href="#" class="btn" :class="(movie.like ? 'btn-outline-danger' : 'btn-light')" 
                                        v-text="movie.textoFav" @click="alternaFav( movie.id )"></a>
                                    <router-link :to="{ name: 'pelicula', params: { id: movie.id } }" class="btn btn-primary">Detalles</router-link>

                                </div>
                                
                            </div>
                            
                        </div>

                    </div>

                    <div class="row">

                        <button v-for="( numero, index ) in paginasTotales" 
                            class="btn m-1" :class="{ 'btn-light': numero != pagina, 'btn-primary': numero == pagina }"
                            v-text="numero" @click="cambioPagina( numero )"></button>

                    </div>

                </div>

                <div v-show="Object.keys(searchMovies).length">

                    <div class="row">
                        
                        <div class="col-12 col-md-6 col-lg-4" v-for="( movie, key ) in searchMovies.results" v-if="movie.poster_path">

                            <div class="card" style="width: 18rem;" :id="movie.id">

                                <img class="card-img-top" :src="prefijoImagen + movie.poster_path">

                                <div class="card-body">

                                    <h3 class="card-title" v-text="movie.title"></h3>
                                    <p class="card-text" v-text="movie.overview.substr( 0, 50 )"></p>
                                    <!--- <a href="#" class="btn" :class="(movie.like ? 'btn-outline-danger' : 'btn-light')" 
                                        v-text="movie.textoFav" @click="alternaFav( movie.id )"></a> --->
                                    <router-link :to="{ name: 'pelicula', params: { id: movie.id } }" class="btn btn-primary">Detalles</router-link>

                                </div>
                                
                            </div>
                            
                        </div>

                    </div>

                    <div class="row">

                        <button v-for="( numero, index ) in searchMovies.total_pages" 
                            class="btn m-1" :class="{ 'btn-light': numero != searchMovies.page, 'btn-primary': numero == searchMovies.page }"
                            v-text="numero" @click="$refs.searchComp.cambioPagina( numero )"></button>

                    </div>

                </div>    

            </div>`,//fin-template

data () {

    return{

        movies: [],
        prefijoImagen: "https://image.tmdb.org/t/p/w185_and_h278_bestv2",
        pagina: 1,
        paginasTotales: null,
        searchMovies: {}

    }//fin-data-return

},//fin-data

methods: {

        getPopularMovies () {

            let urlGenerada = url + 'discover/movie?sort_by=popularity.desc' + '&api_key=' + APIKey
            + '&page=' + this.$data.pagina;

            fetch( urlGenerada ).then( res => res.json() ).
            then( ( {results, page, total_pages} ) => {

                results.forEach( movie => {

                    movie.like = false;
                    movie.textoFav = "Añadir a Favoritos";
    
                } )
                this.movies = results;
                this.paginasTotales = total_pages;
                //console.log( this );

            } );//fin-fetch

        },//fin-getPopularMovies

        alternaFav( id ) {

            this.$data.movies.forEach( movie => {

                if( movie.id == id ){

                    movie.like = !movie.like;
                    movie.textoFav = movie.like ? "Favorita" : "Añadir a Favoritos";

                }//fin-if

            } );//fin-forEach        
            
        },//fin-alternaFav

        cambioPagina( numPagina ){

            this.pagina = numPagina;
            this.getPopularMovies();

        }//fin-cambioPagina

    },//fin-methods
    
    mounted(){
        
        let locationURL = new URL( window.location.href );
        this.$data.pagina = locationURL.searchParams.get( 'pagina' ) || 1;
        this.getPopularMovies();
        
    },//fin-mounted
    
    components: {
    
        SearchComp
    
    }//fin-components

} );//FIN-MovieApp
