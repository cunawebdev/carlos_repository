
  document.getElementById( 'generar-nombre' ).addEventListener( 'submit', generarURL );

  function generarURL( e ){

    e.preventDefault();

    const pais = document.getElementById( 'origen' );
    const paisSeleccionado = pais.options[ pais.selectedIndex ].value;

    const genero = document.getElementById( 'genero' );
    const generoSeleccionado = genero.options[ genero.selectedIndex ].value;

    const cantidad = document.getElementById( 'numero' ).value;

    let url = 'http://uinames.com/api/?';

    if( paisSeleccionado !== '' ){

      url += 'region=' + paisSeleccionado + '&';

    }//fin-if

    if( generoSeleccionado !== '' ){

      url += 'gender=' + generoSeleccionado + '&';

    }//fin-if

    if( cantidad !== '' ){

      url += 'amount=' + cantidad;

    }//fin-if

    //cargarNombresAjax( url );
    cargarNombresFetch( url );

  }//fin-generarURL


  function cargarNombresFetch( url ){

    fetch( url ).then( function( res ){

      return ( res.json() );

    } ).then( function( dataRes ){

      //Se genera el HTML de respuesta
      let htmlResultado = '<h2>Nombres Generados</h2>';

      htmlResultado += "<ul class=\'lista\'>";

      //se agrega el nombre desde el arreglo de JSON
      dataRes.forEach( function( nombre ){

        htmlResultado += '<li>' + nombre.name + '</li>';

      } );//fin-forEach

      htmlResultado += '</ul>';
      //Se imprime el resultado
      document.getElementById( 'resultado' ).innerHTML = htmlResultado;

    } );//fin-fetch

  }//fin-cargarNombresFetch


  function cargarNombresAjax( url ){

    //Se instancia el XMLHttpRequest
    const xhr = new XMLHttpRequest();
    //Se abre la conexion
    xhr.open( 'GET', url, true );

    xhr.onload = function(){

      if( this.status === 200 ){

        const nombres = JSON.parse( this.responseText );
        //Se genera el HTML de respuesta
        let htmlResultado = '<h2>Nombres Generados</h2>';

        htmlResultado += "<ul class=\'lista\'>";

        //se agrega el nombre desde el arreglo de JSON
        nombres.forEach( function( nombre ){

          htmlResultado += '<li>' + nombre.name + '</li>';

        } );//fin-forEach

        htmlResultado += '</ul>';
        //Se imprime el resultado
        document.getElementById( 'resultado' ).innerHTML = htmlResultado;

      }//fin-if

    }//fin-function-onload
    //Envia el Request
    xhr.send();

  }//fin-cargarNombresAjax
