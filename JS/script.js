
  function Muestra_OcultaModal( id ){

    if( document.getElementById ){

      var aux = document.getElementById( id );

      aux.style.display = ( aux.style.display == 'none' ) ? 'block' : 'none';

    }//fin-if

  }//fin-Muestra_OcultaModal


  class Articulo{

    constructor( id, nombre, precio, desc, provNombre, provApellido, provTelefono, provDirLugar, provDirCiudad ){

      this.id = id || -1;
      this.nombre = nombre || "";
      this.precio = precio || 0;
      this.descripcion = desc || "";

      var fecha = new Date();
      this.fechaRegistro = ( fecha.getDate() + "/" + ( fecha.getMonth() + 1 ) + "/" + fecha.getFullYear() );

      this.proveedor = {

        nombre: provNombre || "",
        apellidos: provApellido || "",
        telefono: provTelefono || "",
        direccion: {

          lugar: provDirLugar || "",
          ciudad: provDirCiudad || ""

        }//fin-obj-direccion

      }//fin-obj-proveedor

    }//fin-constructor


    imprimirInfo(){

      if( this.nombre !== "" && this.descripcion !== "" ){
        console.log( "Producto: " + this.nombre + " " + this.descripcion );
      }//fin-if

      if( this.proveedor.nombre !== "" && this.proveedor.apellidos !== "" ){
        console.log( "Proveedor: " + this.proveedor.nombre + " " + this.proveedor.apellidos );
      }//fin-if

      if( this.proveedor.telefono !== "" ){
        console.log( "Telefono: " + this.proveedor.telefono );
      }//fin-if

      if( this.proveedor.direccion.lugar !== "" ){
        console.log( "Direccion: " + this.proveedor.direccion.lugar );
      }//fin-if

      console.log( "Fecha de registro: " + this.fechaRegistro );

    }//fin-function-imprimirInfo

  }//fin-obj-articulo


  var objCoca = new Articulo( undefined, "Coca Cola", 16, "Refreco 365 mL", "Ramon", "Perez", "9802123", "Av. Ejercito Mexicano #782", "Mazatlan" );
  objCoca.imprimirInfo();

  var objFanta = new Articulo( 2, "Fanta", 15, "Refresco 600 mL" );
  objFanta.imprimirInfo();
