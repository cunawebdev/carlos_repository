
  function crearCookie( nombre, valor ){

    valor = escape( valor );

    var hoy = new Date();
    hoy.setMonth( hoy.getMonth() + 1 );

    document.cookie = nombre + "=" + valor + ";expires=" + hoy.toUTCString() + ";";

  }//fin-crearCookie

  function borrarCookie( nombre ){

    var hoy = new Date();
    hoy.setMonth( hoy.getMonth() - 1 );

    document.cookie = nombre + "=x;expires=" + hoy.toUTCString() + ";";

  }//fin-borrarCookie

  function getCookie( nombre ){

    var cookies = document.cookie;

    var arregloCookies = cookies.split( "; " );
    //console.log( arregloCookies );

    for( var i = 0 ; i < arregloCookies.length ; i++ ){

      var parCookies = arregloCookies[ i ].split( "=" );
      arregloCookies[ i ] = parCookies;

      if( parCookies[ 0 ] === nombre ){

        return ( unescape( parCookies[ 1 ] ) );

      }//fin-if

    }//fin-for

    return undefined;

  }//fin-borrarCookie

  crearCookie( "apellido", "Rodriguez" );
  crearCookie( "edad", "21" );
  crearCookie( "email", "xrodriguez@gmail.com" );
  //borrarCookie( "apellido" );

  console.log( getCookie( 'edad' ) );
