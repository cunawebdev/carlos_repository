
  var DB;

  const formulario = document.querySelector( 'form' );
  const mascota = document.querySelector( '#mascota' );
  const cliente = document.querySelector( '#cliente' );
  const telefono = document.querySelector( '#telefono' );
  const fecha = document.querySelector( '#fecha' );
  const hora = document.querySelector( '#hora' );
  const sintomas = document.querySelector( '#sintomas' );
  const citas = document.querySelector( '#citas' );
  const encabezado = document.querySelector( '#administra' );

  // evento que espera que se trate de enviar la informacion
  formulario.addEventListener( 'submit', agregarDatos );
  // evento que se ejecuta cuando el DOM cargue
  document.addEventListener( 'DOMContentLoaded', () => {

    // crea la base de datos de indexedDB
    let crearDB = window.indexedDB.open( 'citas', 1 );
    // en caso de exito
    crearDB.onsuccess = () => {

      console.log( 'Todo bien' );

      DB = crearDB.result;
      mostrarCitas();

    }//crearDB.onsuccess
    // en caso de error
    crearDB.onerror = () => {

      console.log( 'Error' );

    }//fin-crearDB.onerror

    crearDB.onupgradeneeded = ( evento ) => {

      let objectStore = evento.target.result.createObjectStore( 'citas', { keyPath: 'key', autoIncrement: true } );

      objectStore.createIndex( 'mascota', 'mascota', { unique: false } );
      objectStore.createIndex( 'cliente', 'cliente', { unique: false } );
      objectStore.createIndex( 'telefono', 'telefono', { unique: false } );
      objectStore.createIndex( 'fecha', 'fecha', { unique: false } );
      objectStore.createIndex( 'hora', 'hora', { unique: false } );
      objectStore.createIndex( 'sintomas', 'sintomas', { unique: false } );

      // console.log( 'actualizada' );

    }//fin-crearDB.onupgradeneeded

  } );//fin-addEventListener-DOMContentLoaded

  function agregarDatos( evento ){

    evento.preventDefault();

    const nuevaCita = {

      mascota: mascota.value,
      cliente: cliente.value,
      telefono: telefono.value,
      fecha: fecha.value,
      hora: hora.value,
      sintomas: sintomas.value

    }//fin-obj-nuevaCita

    let transaction = DB.transaction( [ 'citas' ], 'readwrite' );

    let peticion = transaction.objectStore( 'citas' ).add( nuevaCita );

    console.log( peticion );

    peticion.onsuccess = () => {

      formulario.reset();

    }//fin-peticion.onsuccess

    transaction.oncomplete = () => {

      console.log( 'Hecho' );
      mostrarCitas();

    }//fin-transaction.oncomplete

    transaction.onerror = () => {

      console.log( 'Error' );

    }//fin-transaction.onerror

  }//fin-agregarDatos

  function mostrarCitas(){
    // limpia las citas anteriores
    while( citas.firstChild ){

      citas.removeChild( citas.firstChild );

    }
    // creamos un object store de 'citas'
    let objectStore = DB.transaction( 'citas' ).objectStore( 'citas' );
    // retorna una peticion que itera entre los datos guardados
    objectStore.openCursor().onsuccess = ( evento ) => {
      // variable auxiliar para tenerla ubicada en los registros y acceder a los datos
      let cursor = evento.target.result;
      // si existe algun registro lo mostramos
      if( cursor ){

        let citaHTML = document.createElement( 'li' );

        citaHTML.setAttribute( 'data-cita-id', cursor.value.key );
        citaHTML.classList.add( 'list-group-item' );
        citaHTML.innerHTML =
          '<p class=\"font-weight-bold\">Mascota: <span class=\"font-weight-normal\">' + cursor.value.mascota + '</span></p>' +
          '<p class=\"font-weight-bold\">Cliete: <span class=\"font-weight-normal\">' + cursor.value.cliente + '</span></p>' +
          '<p class=\"font-weight-bold\">Telefono: <span class=\"font-weight-normal\">' + cursor.value.telefono + '</span></p>' +
          '<p class=\"font-weight-bold\">Fecha: <span class=\"font-weight-normal\">' + cursor.value.fecha + '</span></p>' +
          '<p class=\"font-weight-bold\">Hora: <span class=\"font-weight-normal\">' + cursor.value.hora + '</span></p>' +
          '<p class=\"font-weight-bold\">Sintomas: <span class=\"font-weight-normal\">' + cursor.value.sintomas + '</span></p>';

        // boton para borrar registros individuales
        const botonBorrar = document.createElement( 'button' );
        botonBorrar.classList.add( 'borrar', 'btn', 'btn-danger' );
        botonBorrar.innerHTML = '<span aria-hidden=\"true\">x</span> Borrar';
        botonBorrar.onclick = borrarCita;
        citaHTML.appendChild( botonBorrar );
        // agregar al padre del DOM
        citas.appendChild( citaHTML );
        // procede al siguinte registro
        cursor.continue();

      }else{

        if( !citas.firstChild ){
          // en caso de que no haya citas registradas este será el encabezado
          encabezado.textContent = 'Ninguna cita para mostrar';
          let listado = document.createElement( 'p' );
          listado.classList.add( 'text-center' );
          listado.textContent = 'Agregar citas para comezar';
          citas.appendChild( listado );

        }else {
          // si existen citas registradas se cambia el encabezado
          encabezado.textContent = 'Administra tus citas';

        }//fin-if-else

      }//fin-if-else

    }//fin-objectStore.openCursor().onsuccess

  }//fin-mostrarCitas

  function borrarCita( evento ){
    // mostramos el nombre de la mascota que se eliminara el registro para confirmar
    if( confirm( "¿Eliminar registro de la " + evento.target.parentElement.firstChild.textContent + "?" ) ){
      // obtenemos el ID del registro al que se le dio clic para eliminar
      let citaID = Number( evento.target.parentElement.getAttribute( 'data-cita-id' ) );

      // transaccion para eliminar
      let transaction = DB.transaction( [ 'citas' ], 'readwrite' );
      // peticion para eliminar el registro
      let peticion = transaction.objectStore( 'citas' ).delete( citaID );

      transaction.oncomplete = () => {

        evento.target.parentElement.parentElement.removeChild( evento.target.parentElement );
        alert( 'Eliminado' );
        mostrarCitas();

      }//fin-transaction.oncomplete

    }//fin-if-confirm

  }//fin-borrarCita
