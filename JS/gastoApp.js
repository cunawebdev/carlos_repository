
  var presupuesto = prompt( 'Cual es el presupuesto semanal?' );
  const formulario = document.getElementById( 'agregar-gasto' );

  class Presupuesto {

    constructor( presupuesto ) {

      this.presupuesto = Number( presupuesto );
      this.restante = Number( presupuesto );

    }//fin-constructor

    presupuestoRestante(cantidad = 0) {

      return this.restante -= Number( cantidad );

    }//fin-presupuestoRestante

  }//fin-class-Presupuesto

  class Interfaz{

    insertarPresupuesto( cantidad ) {

      const presupuestoSpan = document.querySelector( 'span#total' );
      const restanteSpan = document.querySelector( 'span#restante' );

      // Insertar al HTML
      presupuestoSpan.innerHTML = `${cantidad}`;
      restanteSpan.innerHTML = `${cantidad}`;

    }//fin-insertarPresupuesto

    imprimirMensaje( mensaje, tipo ) {

      const divMensaje = document.createElement( 'div' );
      divMensaje.classList.add( 'text-center', 'alert' );
      if( tipo === 'error' ) {

        divMensaje.classList.add( 'alert-danger' );

      } else {

        divMensaje.classList.add( 'alert-success' );

      }//fin-if-else

      divMensaje.appendChild( document.createTextNode( mensaje ) );
      // Insertar en el div primario antes del formulario
      document.querySelector( '.primario' ).insertBefore( divMensaje, formulario );

      // Quitar el mensaje despues de 2 segundos
      setTimeout( function() {

        document.querySelector( '.primario .alert' ).remove();

      }, 2000 );

    }//fin-imprimirMensaje

    agregarGastoListado( nombre, cantidad ){

      const gastosListado = document.querySelector( '#gastos ul' );
      const li = document.createElement( 'li' );

      li.className = 'list-group-item d-flex justify-content-between align-items-center';

      // Insertar el gasto
      li.innerHTML = `
      ${nombre}
      <span class="badge badge-primary badge-pill"> $ ${cantidad} </span>
      `;
      gastosListado.appendChild( li );

    }//fin-agregarGastoListado

    presupuestoRestante( cantidad ){

      const restante = document.querySelector( 'span#restante' );
      // Leemos el presupuesto restante
      const presupuestoRestanteUsuario =  cantidadPresupuesto.presupuestoRestante(cantidad);

      restante.innerHTML = `${presupuestoRestanteUsuario}`;

      this.comprobarPresupuesto();

    }

    comprobarPresupuesto() {

         const presupuestoTotal = cantidadPresupuesto.presupuesto;
         const presupuestoRestante = cantidadPresupuesto.restante;

         if( ( presupuestoTotal / 4 ) > presupuestoRestante){

              const restante = document.querySelector( '.restante' );
              restante.classList.remove( 'alert-success', 'alert-warning' );
              restante.classList.add( 'alert-danger' );

         } else if( ( presupuestoTotal / 2 ) > presupuestoRestante ) {

              const restante = document.querySelector( '.restante' );
              restante.classList.remove( 'alert-success' );
              restante.classList.add( 'alert-warning' );

         }//fin-if-else

    }//fin-comprobarPresupuesto

  }//fin-class-Interfaz



  document.addEventListener( 'DOMContentLoaded', function(){

    if( presupuesto === null || presupuesto === "" ){

      window.location.reload();

    }else {

      cantidadPresupuesto = new Presupuesto(presupuesto);
      const ui = new Interfaz();

      ui.insertarPresupuesto( cantidadPresupuesto.presupuesto );

    }//fin-if-else

  } );  //fin-Listener

  formulario.addEventListener( 'submit', function( e ){

    e.preventDefault();

    const nombreGasto = document.querySelector( '#gasto' ).value;
    const cantidadGasto = document.querySelector( '#cantidad' ).value;
    const ui = new Interfaz();

    if(nombreGasto === '' || cantidadGasto === '') {

         ui.imprimirMensaje( 'Hubo un error', 'error' );

    }else{

      ui.imprimirMensaje( 'Hecho', 'correcto' );
      ui.agregarGastoListado( nombreGasto, cantidadGasto );
      ui.presupuestoRestante( cantidadGasto );

    }//fin-if-else

    formulario.reset();

  } );//fin-Listener
